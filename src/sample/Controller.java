package sample;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Controller {
    @FXML
    private AnchorPane anchorPane;
    private FileChooser fileChooser = new FileChooser();


    public void clickUploadSimpleFileButton() throws IOException {

        fileChooser.setTitle("open file");
        /**
         * pour appeler stage dans controller
         */
        Stage stage = (Stage)anchorPane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        if (file!=null){
            Desktop desktop = Desktop.getDesktop();
            desktop.open (file);
        }

    }
    public void clickUploadMultipleFileButton(){
        fileChooser.setTitle("open file");
        Stage stage = (Stage)anchorPane.getScene().getWindow();
        List<File> list = fileChooser.showOpenMultipleDialog(stage);
        if (list!=null){
            for(File file:list){
                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.open (file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }


    }
}



